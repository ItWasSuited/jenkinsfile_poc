
class MiClazz implements Serializable {

    String field
    transient String transientField

    MiClazz(String field) {
        this.field = field
        this.transientField = "initial val for t. field"
    }

    @NonCPS
    private void readObject(ObjectInputStream i) {
        i.defaultReadObject()
        transientField = "Deserialized"
    }

}

class MyCpsContext implements Serializable {
    final context

    MyCpsContext(context) {
        this.context = context
    }

    String getEnv(String key) {
        return context.env."${key}"
    }

    def stage(String name, Closure closure) {
        return context.stage(name, closure)
    }
}

def cpsContext = new MyCpsContext(this)

def toStr(MiClazz o) {
    return "Field: " + o.field + "TransientField: " + o.transientField
}

def o = new MiClazz('abc')

node {
    catchError {
        cpsContext.stage 'First stage', {
            echo "Field: ${toStr(o)}"
            def b = cpsContext.getEnv('BUILD_NUMBER')
            echo "Build: " + b
            input(message: 'Introduce something...')
            echo "Field: ${toStr(o)}"
            echo "Build: ${env.BUILD_NUMBER}"
        }
    }
}
